package tekmob.proyek.CariRoti.service;

import tekmob.proyek.CariRoti.model.RotiModel;
import tekmob.proyek.CariRoti.model.UserModel;

import java.util.List;

public interface RotiService {
    RotiModel addRoti(RotiModel roti, UserModel user);
    RotiModel updateRoti(RotiModel roti, UserModel user);
    RotiModel getRoti(Long rotiId);
    List<RotiModel> getListRoti(UserModel user);
}
