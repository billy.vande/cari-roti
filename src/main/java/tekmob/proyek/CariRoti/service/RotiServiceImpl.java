package tekmob.proyek.CariRoti.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tekmob.proyek.CariRoti.model.RotiModel;
import tekmob.proyek.CariRoti.model.UserModel;
import tekmob.proyek.CariRoti.repository.RotiDB;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class RotiServiceImpl implements RotiService{

    @Autowired
    RotiDB rotiDB;
    @Override
    public RotiModel addRoti(RotiModel roti, UserModel user) {
        roti.setUser(user);
        return rotiDB.save(roti);
    }

    @Override
    public RotiModel updateRoti(RotiModel roti, UserModel user) {
//        RotiModel rotiPlaceholder = rotiDB.findRotiByRotiId(roti.getRotiId());
        return null;
    }

    @Override
    public RotiModel getRoti(Long rotiId) {
        return null;
    }

    @Override
    public List<RotiModel> getListRoti(UserModel user) {
        return rotiDB.findRotiByUser(user);
    }
}
