package tekmob.proyek.CariRoti.service;

import org.springframework.security.core.userdetails.User;
import tekmob.proyek.CariRoti.model.UserModel;
import tekmob.proyek.CariRoti.repository.UserDB;
import tekmob.proyek.CariRoti.model.RotiModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class UsersServiceImpl implements UsersService{

    @Autowired
    private UserDB userDB;

    @Override
    public UserModel addUser(UserModel user) {
        String pass = encrypt(user.getPassword());
        user.setPassword(pass);
        return userDB.save(user);
    }

    @Override
    public String encrypt(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String passwordHash = passwordEncoder.encode(password);
        return passwordHash;
    }

    @Override
    public UserModel getUserByUsername(String Username) {
        return userDB.findByUsername(Username);
    }

}
