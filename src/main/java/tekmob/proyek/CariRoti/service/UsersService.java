package tekmob.proyek.CariRoti.service;

import tekmob.proyek.CariRoti.model.UserModel;
import tekmob.proyek.CariRoti.model.RotiModel;

public interface UsersService {
    UserModel addUser(UserModel user);
    String encrypt(String password);
    UserModel getUserByUsername(String Username);
//    UserModel inactive(UserModel user, boolean isActive);
}
