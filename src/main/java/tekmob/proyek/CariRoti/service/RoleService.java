package tekmob.proyek.CariRoti.service;

import tekmob.proyek.CariRoti.model.RoleModel;
import java.util.List;

public interface RoleService {
    List<RoleModel> getListRole();
}
