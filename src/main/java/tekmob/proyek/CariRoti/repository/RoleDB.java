package tekmob.proyek.CariRoti.repository;

import tekmob.proyek.CariRoti.model.RoleModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleDB extends JpaRepository<RoleModel, Long> {
    Optional<RoleModel> findRoleByIdRole(Long idRole);
}
