package tekmob.proyek.CariRoti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tekmob.proyek.CariRoti.model.RotiModel;
import tekmob.proyek.CariRoti.model.UserModel;

import java.util.List;
import java.util.Optional;

@Repository
public interface RotiDB extends JpaRepository<RotiModel, Long> {
    Optional<RotiModel> findRotiByRotiId(Long rotiId);
    List<RotiModel> findRotiByUser(UserModel user);
}
