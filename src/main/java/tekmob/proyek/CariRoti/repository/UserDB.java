package tekmob.proyek.CariRoti.repository;

import tekmob.proyek.CariRoti.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface UserDB extends JpaRepository<UserModel, Long> {
    UserModel findByUsername(String username);
    Boolean existsByUsername(String username);
}
