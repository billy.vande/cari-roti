package tekmob.proyek.CariRoti.controller;

import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import tekmob.proyek.CariRoti.model.RoleModel;
import tekmob.proyek.CariRoti.model.UserModel;
import tekmob.proyek.CariRoti.service.RoleService;
import tekmob.proyek.CariRoti.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tekmob.proyek.CariRoti.repository.UserDB;
import tekmob.proyek.CariRoti.repository.RoleDB;

@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping("/user")
public class UserRestController {

    String huruf = ".*[A-Z].*";
    String angka = ".*[0-9].*";
    String hurufBesar = ".*[A-Z].*";
    String hurufKecil = ".*[a-z].*";
    String special = ".*[^A-Za-z0-9].*";

    @Autowired
    UserDB userDB;

    @Autowired
    RoleDB roleDB;

    @Autowired
    UsersService usersService;

    @Autowired
    RoleService roleService;

    @GetMapping("/add/")
    public String addUserFormPage(
            Model model
    ) {
        UserModel user = new UserModel();
        List<RoleModel> listRole = roleService.getListRole();
        model.addAttribute("user", user);
        model.addAttribute("listRole", listRole);
        return "form-add-user";
    }

    @PostMapping(value = "/add/")
    public String addUserSubmit(
            @ModelAttribute UserModel user,
            RedirectAttributes red
    ) {
//        String password = user.getPassword();
//        if (password.length() < 8) {
//            red.addFlashAttribute("pesanError", "Password Harus Minimal 8 Karakter");
//            return "redirect:/user/add";
//        }
//        if (!password.matches(hurufBesar) || !password.matches(hurufKecil) ||
//                !password.matches(angka) || !password.matches(special)) {
//            red.addFlashAttribute("pesanError", "Password harus mengandung angka, huruf, dan simbol");
//            return "redirect:/user/add";
//        }
        usersService.addUser(user);
        return "redirect:/"; //disini ada yang diubah
    }


    @GetMapping("/{username}/")
    public String getUserProfile(
            @PathVariable String username,
            Authentication auth,
            Model model
    ){
        UserModel user = usersService.getUserByUsername(username);
        if(!auth.getName().equals(user.getUsername())) {
            return "error/403";
        }
        model.addAttribute("name", user.getNama());
        model.addAttribute("user", user);
        return "profile";
    }

//    @PostMapping("/user/inactive/{noHP}")
//    public ResponseEntity<UserModel> inactiveUser(
//            @PathVariable String email
//    ){
//        UserModel users = usersService.getUserByNoHP(noHP);
//        usersService.inactive(users,false);
//        if(users == null) {
//            return new ResponseEntity(HttpStatus.NOT_FOUND);
//        }
//        return new ResponseEntity<UserModel>(users, HttpStatus.OK);
//    }

//    @PutMapping("user/{email}/update")
//    public UserModel updateUser(
//            @PathVariable("noHP") String noHP,
//            @RequestBody SignupRequest signUpRequest
//    ) {
//        try {
//            return usersService.updateUser(
//                    noHP,
//                    roleDB.findByNamaDept(signUpRequest.getDepartemen()),
//                    signUpRequest.getNamaUser(),
//                    signUpRequest.getJenisKelamin(),
//                    signUpRequest.getPassword());
//        } catch (NoSuchElementException e) {
//            throw new ResponseStatusException(
//                    HttpStatus.NOT_FOUND, "User with phone no " + noHP + " Not Found."
//            );
//        }
//    }

//    @GetMapping("/user")
//    private Response<List<UserDTO>> getAllUser(@RequestParam(value = "nama", defaultValue = "") String nama){
//        Response<List<UserDTO>> response =new Response<>();
//        response.setStatus(200);
//        response.setMessage("success");
//        response.setResult(usersService.getAllUser(nama));
//        return response;
//    }
}
