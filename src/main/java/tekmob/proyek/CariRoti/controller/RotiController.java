package tekmob.proyek.CariRoti.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tekmob.proyek.CariRoti.model.RotiModel;
import tekmob.proyek.CariRoti.service.RotiServiceImpl;
import tekmob.proyek.CariRoti.service.UsersServiceImpl;
import tekmob.proyek.CariRoti.model.RoleModel;

import java.util.List;

@Controller
@RequestMapping("/roti")
public class RotiController {

    @Qualifier("rotiServiceImpl")
    @Autowired
    RotiServiceImpl rotiService;

    @Qualifier("usersServiceImpl")
    @Autowired
    UsersServiceImpl usersService;

    @GetMapping("/")
    public String showTukangRotiMap() {
        return "map-roti";
    }

    @GetMapping(value = "/{username}/")
    public String listRoti(
            @PathVariable String username,
            Model model
    ){
        List<RotiModel> listRoti = rotiService.getListRoti(usersService.getUserByUsername(username));
        String role = usersService.getUserByUsername(username).getRole().getRole();
        model.addAttribute("listRoti", rotiService.getListRoti(usersService.getUserByUsername(username)));
        model.addAttribute("username", username);
        return "list-roti";
    }

    @GetMapping(value = "/{username}/add/")
    public String addRotiForm(
            Model model,
            @PathVariable String username
    ){
        RotiModel roti = new RotiModel();
        model.addAttribute("roti", roti);
        model.addAttribute("username" ,username);
        return "tambah-roti";
    }

    @PostMapping(value = "/{username}/add/")
    public String addRoti(
            @ModelAttribute RotiModel roti,
            @PathVariable String username
    ){
        rotiService.addRoti(roti,usersService.getUserByUsername(username));
        return "redirect:/roti/{username}/";
    }

    @GetMapping(value = "/{username}/{rotiId}/")
    public String readRoti(
            Model model,
            @PathVariable String username,
            @PathVariable Long rotiId
            ){
        RotiModel roti = rotiService.getRoti(rotiId);
        model.addAttribute("roti", roti);
        return "roti-satuan";
    }

    @PostMapping(value = "/{username}/{rotiId}/update/")
    public String updateRoti(
            @ModelAttribute RotiModel roti,
            @PathVariable String username,
            @PathVariable Long rotiId
    ){
        rotiService.updateRoti(roti, usersService.getUserByUsername(username));
        return "redirect:/roti/{username}/{rotiId}/";
    }

    @GetMapping(value = "/{username}/updateStok/")
    public String updateStokRotiForm(
            @ModelAttribute RotiModel roti,
            @PathVariable String username
    ){
        rotiService.getListRoti(usersService.getUserByUsername(username));
        rotiService.updateRoti(roti, usersService.getUserByUsername(username));
        return "update-stok";
    }

//    @GetMapping(value = "/{username}/updateStok/")
//    public String updateStokRotiSubmit(
//            @ModelAttribute RotiModel roti,
//            @PathVariable String username
//    ){
//        rotiService.getListRoti(usersService.getUserByUsername(username));
//        return "redirect:/roti/{username}/";
//    }

}
