package tekmob.proyek.CariRoti.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import tekmob.proyek.CariRoti.model.RoleModel;
import tekmob.proyek.CariRoti.service.UsersServiceImpl;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;

import java.util.Collection;

@Controller
public class PageController {
    @Qualifier("usersServiceImpl")
    @Autowired
    UsersServiceImpl usersService;

    @RequestMapping("")
    public String login(
            Authentication auth
    ) {
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        try {

            System.out.println(authorities.toString().substring(1, authorities.toString().length() - 1));
            String username = auth.getName();
            System.out.println(username);
            if (authorities.toString().substring(1, authorities.toString().length() - 1).equalsIgnoreCase("PEMBELI")) {
                return "redirect:/roti/";
            }
            return "redirect:/user/"+username+"/";
        } catch (Exception e) {
            return "login";
        }
    }


    @GetMapping("/home")
    public String home(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        String role = usersService.getUserByUsername(username).getRole().getRole();
        model.addAttribute("username", username);
        model.addAttribute("role", role);
        return "home";
    } //disini ada yang diubah

}
