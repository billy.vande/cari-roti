package tekmob.proyek.CariRoti.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter @Getter
@Entity
@Table(name="roti")
public class RotiModel implements Serializable {

    @Id
    @Column(name="roti_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long rotiId;

    @NotNull
    @Size(max = 20)
    @Column(name="nama_roti", nullable = false)
    private String namaRoti;

    @NotNull
    @Size(max = 20)
    @Column(name="harga_roti", nullable = false)
    private String hargaRoti;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_user", referencedColumnName = "id_user", nullable = false)
    private UserModel user;
}
