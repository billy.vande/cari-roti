package tekmob.proyek.CariRoti.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter @Getter
@Entity
@Table(name="user")
public class UserModel implements Serializable {

    @Id
    @Column(name="id_user")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUser;

    //Relasi dengan Role
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "id_role", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private RoleModel role;

    @NotNull
    @Size(max = 20)
    @Column(name="nama", nullable = false)
    private String nama;

    @NotNull
    @Size(max = 20)
    @Column(name="username", nullable = false)
    private String username;

    @NotNull
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name="password", nullable = false)
    private String password;

    @JsonIgnore
    @OneToMany(mappedBy = "rotiId", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<RotiModel> listRoti;


}
