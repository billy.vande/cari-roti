package tekmob.proyek.CariRoti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CariRotiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CariRotiApplication.class, args);
	}

}